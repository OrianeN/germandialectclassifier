"""Contains functions for predicting dialects on the website.

This project has been developed by Oriane Nédey and is under licence CC BY-NC-SA 4.0 (https://creativecommons.org/licenses/by-nc-sa/4.0/)
Creative Commons Licence : Attribution - Non Commercial - Share Alike 4.0 International
This license lets others remix, tweak, and build upon your work non-commercially, as long as they credit you and license their new creations under the identical terms.
"""

import os
import pickle
import re

from nltk.tokenize import sent_tokenize

from _constants import *
from predict_helpers import threshold_fam, threshold_dial, predict_with_threshold


def import_classifiers(fam_clf=NB_FAM_CLF, dial_clf=NB_DIAL_CLF):
    """
    Imports 2 specific classifiers
    1 - for predicting the dialect family
    2 - for predicting the more precise dialect
    :param fam_clf: string path
    :param dial_clf: string path
    :return: dict {"family": classifier 1, "dialect": classifier 2}
    """
    with open(fam_clf, "rb") as f:
        my_unpickler = pickle.Unpickler(f)
        fam_classifier = my_unpickler.load()
    with open(dial_clf, "rb") as f:
        my_unpickler = pickle.Unpickler(f)
        dial_classifier = my_unpickler.load()
    print("classifiers successfully imported !")
    classifier_dict = {"family": fam_classifier, "dialect": dial_classifier}
    return classifier_dict


def predict_from_string(text, classifier):
    """
    Calculates which dialect and dialect family are the most probable
    for a given text.
    :param text: string (one or more lines)
    :param classifier: dict of Pipeline objects - the trained classifiers {"family", "dialect"}
    :return: list :
    0 = list strings = identified family and dialect (family, dialect)
    1 = dict with all stats (label, frequence, proba) for "family" and "dialect"
    """
    # Splits text into sentences
    sentences = []
    lines = text.split("\n")
    for line in lines:
        line_sentences = clean_line(line)
        for sentence in line_sentences:
            sentences.append((sentence,))

    # Creates a DataFrame with the sentences
    text_df = pd.DataFrame.from_records(sentences, columns=['sentence'])

    # Predicts the dialect family for each sentence, keeping all results + the best result
    X_family = text_df[['sentence']]
    family_pred = predict_with_threshold(classifier['family'], X_family, threshold_fam)

    most_probable_f, family_stats = get_stats(family_pred)

    # Add predicted dialect families to the DataFrame
    text_df['dialect_family'] = family_pred

    # Transform predicted families into features with One Hot Encoding
    dialect_fam_dummies = pd.get_dummies(text_df['dialect_family'])
    X_dialect = pd.concat([text_df[['sentence']], dialect_fam_dummies], axis=1)

    # Predicts dialect for each sentence
    dialect_pred = predict_with_threshold(classifier['dialect'], X_dialect, threshold_dial)

    most_probable_d, dialect_stats = get_stats(dialect_pred)

    # Returns identification results
    best_result = (most_probable_f, most_probable_d)
    all_stats = {"family": family_stats, "dialect": dialect_stats}
    return best_result, all_stats


def clean_line(string):
    """
    This function removes in the given string :
    - tabs, commas, semicolons, quotation marks (avoiding problems with CSV file format)
    - URLs, e-mail addresses, numeric data, special characters
    Furthermore, it cuts the string into sentences.
    :param string: the given character string
    :return: array: all the (cleaned) sentences present in the given string.
    """
    # Removing potential tabs
    string = re.sub(r'\t', ' ', string)
    # Removing URLs, e-mail addresses and numeric data
    string = re.sub(r'https?://[^ ]+', '', string)  # URLs
    string = re.sub(r'[a-zA-Z._-]+@[a-zA-Z._-]+\.\w{2,4}', '', string)  # e-mail addresses
    string = re.sub(r'\b\d([^ \n]*\d)?(\b|g)', '', string)  # g for grams, frequent in cooking recipes
    string = re.sub(r'[\x80-\xA0]', '', string)  # Special chars
    # Removing commas, semicolons and quotation marks
    string = re.sub(r'[,;\'"]', '', string)
    # Sentence segmentation with NLTK
    sentences = sent_tokenize(string)
    return sentences


def get_stats(predictions):
    """
    Calculates stats from a list of predicted labels.
    :param predictions: array of predicted labels
    :return: array :
    - 0 : string. the most probable label from the list
    - 1 : dict of tuples with all stats : {label: frequence, probability}
    """
    pred_nb = len(predictions)
    pred_labels = set(predictions)
    most_probable = (None, 0, 0)
    stats = {}
    for label in pred_labels:
        label_count = predictions.count(label)
        probability_score = 100 * label_count / pred_nb
        stats[label] = (label, label_count, probability_score)
        if label_count > most_probable[1]:
            most_probable = stats[label]
    return most_probable[0], stats


if __name__ == '__main__':
    classifier_nb = import_classifiers()
    texte_to_predict = """Ick bin ein Berliner.
    Wat denkst du ?
    Jeschlajen mit jesenkten Häuptern schlichen Ka
    Dat well ich ens flöck verposementeere."""

    most_probable, all_stats = predict_from_string(texte_to_predict, classifier_nb)

    most_probable_f = most_probable[0]
    most_probable_d = most_probable[1]

    confidence_f = all_stats["family"][most_probable_f][2]
    confidence_d = all_stats["dialect"][most_probable_d][2]

    print("Résultats\n")

    print("Famille de dialectes la plus probable pour ce texte : {}".format(most_probable_f))
    if confidence_f < 100:
        print("Score de confiance : {:.2f}".format(confidence_f))

    print("Dialecte le plus probable pour ce texte : {}".format(most_probable_d))
    if confidence_d < 100:
        print("Score de confiance : {:.2f}".format(confidence_d))
