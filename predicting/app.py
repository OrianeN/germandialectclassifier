"""Contains the Flask web application.

This project has been developed by Oriane Nédey and is under licence CC BY-NC-SA 4.0 (https://creativecommons.org/licenses/by-nc-sa/4.0/)
Creative Commons Licence : Attribution - Non Commercial - Share Alike 4.0 International
This license lets others remix, tweak, and build upon your work non-commercially, as long as they credit you and license their new creations under the identical terms.
"""
import sys
sys.path.append('..')

from flask import Flask, request, render_template

import results_predictor as predictor
from _constants import all_clf_paths

app = Flask(__name__)


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/identification/', methods=['GET', 'POST'])
def identification():
    if request.method == 'GET':
        return render_template("formulaire_identification.html")
    else:
        text = request.form.get('text')  # Texte sécurisé via les accolades {{}} avec Jinja2
        chosen_classifier = request.form.get('classifier')  # Texte sécurisé via les accolades {{}} avec Jinja2
        clf_paths = all_clf_paths[chosen_classifier]

        classifier = predictor.import_classifiers(fam_clf=clf_paths[0], dial_clf=clf_paths[1])
        results, all_stats = predictor.predict_from_string(text, classifier)

        stats_family = all_stats['family']
        stats_dialect = all_stats['dialect']
        result_family = results[0]
        confidence_f = round(stats_family[result_family][2], 2)
        result_dialect = results[1]
        confidence_d = round(stats_dialect[result_dialect][2], 2)

        return render_template("resultats_identification.html",
                               code_clf=chosen_classifier, text=text,
                               stats_family=stats_family, stats_dialect=stats_dialect,
                               result_family=result_family, confidence_f=confidence_f,
                               result_dialect=result_dialect, confidence_d=confidence_d)


@app.route('/projet/')
def present_project():
    return render_template("presentation_projet.html")


if __name__ == "__main__":
    # TODO Retirer debug=True quand le site sera terminé
    app.run(debug=True)
