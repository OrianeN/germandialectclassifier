from app import app

if __name__ == "__main__":
    app.run()
    # Ligne de commande pour lancer le site en production sur Ubuntu : gunicorn --bind 0.0.0.0:5000 wsgi:app
