"""Contains imports and constant variables used in the project.

This project has been developed by Oriane Nédey and is under licence CC BY-NC-SA 4.0 (https://creativecommons.org/licenses/by-nc-sa/4.0/)
Creative Commons Licence : Attribution - Non Commercial - Share Alike 4.0 International
This license lets others remix, tweak, and build upon your work non-commercially, as long as they credit you and license their new creations under the identical terms.
"""

import os

from nltk.tokenize import word_tokenize
from sklearn import metrics
from sklearn import model_selection
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.ensemble import VotingClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import classification_report, f1_score, accuracy_score, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.dummy import DummyClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import LinearSVC
from sklearn.calibration import CalibratedClassifierCV
from sklearn.pipeline import Pipeline, FeatureUnion, make_pipeline
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pickle
import datetime

ROOT = os.path.dirname(__file__)

INPUT_CSV = ROOT + "/training/german_dialects.csv"
UNKNOWN_CORPUS = ROOT + '/training/corpus_test_unknown.csv'

PLOTS = ROOT + '/plots/'

CLASSIFIERS = ROOT + '/classifiers/'

OPTIM_FOLDER = ROOT + '/optimization_reports/'

DUMMY_FAM = CLASSIFIERS + "saved_clf_dummy_fam"
DUMMY_DIAL = CLASSIFIERS + "saved_clf_dummy_dial"

NB_FAM_CLF = CLASSIFIERS + "saved_clf_nb_fam"
NB_DIAL_CLF = CLASSIFIERS + "saved_clf_nb_dialect"

SVM_FAM_CLF = CLASSIFIERS + "saved_clf_svm_fam"
SVM_DIAL_CLF = CLASSIFIERS + "saved_clf_svm_dialect"

E1_SVM_FAM_CLF = CLASSIFIERS + 'saved_clf_e1_svm_fam'
E1_SVM_DIAL_CLF = CLASSIFIERS + 'saved_clf_e1_svm_dial'

E1_NB_FAM_CLF = CLASSIFIERS + 'saved_clf_e1_nb_fam'
E1_NB_DIAL_CLF = CLASSIFIERS + 'saved_clf_e1_nb_dial'

E2_SVM_FAM_CLF = CLASSIFIERS + 'saved_clf_e2_svm_fam'
E2_SVM_DIAL_CLF = CLASSIFIERS + 'saved_clf_e2_svm_dial'

E2_NB_FAM_CLF = CLASSIFIERS + 'saved_clf_e2_nb_fam'
E2_NB_DIAL_CLF = CLASSIFIERS + 'saved_clf_e2_nb_dial'

all_clf_paths = {
    "nb": [NB_FAM_CLF, NB_DIAL_CLF],
    "svm": [SVM_FAM_CLF, SVM_DIAL_CLF],
    "e1-svm": [E1_SVM_FAM_CLF, E1_SVM_DIAL_CLF],
    "e2-svm": [E2_SVM_FAM_CLF, E2_SVM_DIAL_CLF],
    "e1-nb": [E1_NB_FAM_CLF, E1_NB_DIAL_CLF],
    "e2-nb": [E2_NB_FAM_CLF, E2_NB_DIAL_CLF]
}
