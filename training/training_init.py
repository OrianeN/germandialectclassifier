"""Initializes the training process.

This project has been developed by Oriane Nédey and is under licence CC BY-NC-SA 4.0 (https://creativecommons.org/licenses/by-nc-sa/4.0/)
Creative Commons Licence : Attribution - Non Commercial - Share Alike 4.0 International
This license lets others remix, tweak, and build upon your work non-commercially, as long as they credit you and license their new creations under the identical terms.
"""

from training.training_helpers import *
from predict_helpers import threshold_fam, threshold_dial

# Prepare the initial DataFrame from a CSV file
df = create_df_from_csv(INPUT_CSV)

# Extraction of the data for the classification tasks (families and dialects)
X_family = df[['sentence']]
y_family = df.dialect_family

dialect_fam_dummies = pd.get_dummies(df['dialect_family'])
X_dialect = pd.concat([df[['sentence']], dialect_fam_dummies], axis=1)
y_dialect = df['dialect']

# Test data for the threshold
df_unknown = create_df_from_csv(UNKNOWN_CORPUS)
X_fam_unknown = df_unknown[['sentence']]
y_fam_unknown = df_unknown.dialect_family

dialect_fam_dummies_unknown = pd.get_dummies(df_unknown['dialect_family'])
X_dialect_unknown = pd.concat([df_unknown[['sentence']], dialect_fam_dummies_unknown], axis=1)
y_dialect_unknown = df_unknown['dialect']

# String descriptors (for files and plots naming)
data_desc_fam = "grandes familles de dialectes"
data_desc_dialects = "variétés locales ou régionales (dialectes)"

# Dictionaries that gathers all useful variables at once
family_vars = {
    "X": X_family,
    "y": y_family,
    "data_desc": data_desc_fam,
    "threshold": threshold_fam,
    "X_test_unknown": X_fam_unknown,
    "y_test_unknown": y_fam_unknown,
    "datatype": "fam",
}

dialect_vars = {
    "X": X_dialect,
    "y": y_dialect,
    "data_desc": data_desc_dialects,
    "threshold": threshold_dial,
    "X_test_unknown": X_dialect_unknown,
    "y_test_unknown": y_dialect_unknown,
    "datatype": "dial",
}

# Lists for the base estimators
models = [MultinomialNB(), CalibratedClassifierCV(LinearSVC(penalty='l2'))]
model_names = ["Naive Bayes (MultinomialNB)", "SVM (LinearSVC)"]


if __name__ == '__main__':
    plot_dialects_df(df)  # Show the corpus' stats with figures and nice plots