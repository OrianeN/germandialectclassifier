"""Program for training simple classifiers using Naive Bayes, SVM or DummyClassifier.

This project has been developed by Oriane Nédey and is under licence CC BY-NC-SA 4.0 (https://creativecommons.org/licenses/by-nc-sa/4.0/)
Creative Commons Licence : Attribution - Non Commercial - Share Alike 4.0 International
This license lets others remix, tweak, and build upon your work non-commercially, as long as they credit you and license their new creations under the identical terms.
"""

from training.training_helpers import *

# Preparing initial data
from training.training_init import family_vars, dialect_vars, models, model_names

from training.models_manager import save_clf, get_optimizing_command, get_model_command, get_data_command, save_report


# Fonction pour entrainer les classifieurs simples avec MultinomialNB ou SVM
def make_simple_clf(output_path, classifier_to_train, model_name, family_data, prep_params):
    """
    Entraine un classifieur
    :param output_path: string. chemin du fichier où sauvegarder le modèle entrainé
    :param classifier_to_train: fonction implémentant fit et predict_proba. modèle à entrainer
    :param model_name: string. nom du modèle à entrainer (pour affichage sur les graphiques)
    :param family_data: boolean. True si c'est l'apprentissage de la classification des familles de dialectes. False si c'est pour apprendre les dialectes plus précis.
    :param prep_params: dict. Dictionnaire des paramètres pour créer la pipeline de preprocessing
    :return: None
    """

    if family_data is True:
        vars_dict = family_vars

    else:
        vars_dict = dialect_vars

    preprocess_pipeline, words_pipeline, chars_pipeline = make_preprocess_pipeline(w_min_df=prep_params['w_min_df'],
                                                                                   w_ngram_range=prep_params['w_ngram_range'],
                                                                                   c_min_df=prep_params['c_min_df'],
                                                                                   c_ngram_range=prep_params['c_ngram_range'])

    X_train, X_test, y_train, y_test = train_test_split(vars_dict['X'], vars_dict['y'], test_size=0.2)
    X_test, y_test = add_unknown_data_to_test(X_test, y_test, vars_dict['X_test_unknown'], vars_dict['y_test_unknown'])

    classifier_pipeline = Pipeline([("preprocess", preprocess_pipeline),
                                    ("clf", classifier_to_train)])

    classifier_pipeline.fit(X_train, y_train)

    infos_dict = infos_features(words_pipeline, chars_pipeline)
    clf_name = "{} ({})".format(model_name, vars_dict['data_desc'])
    evaluate_model(classifier_pipeline, X_test, y_test, clf_name, prep_params, infos_dict, proba_threshold=vars_dict['threshold'])

    save_clf(classifier_pipeline, output_path)

# Listes des chemins de sortie pour la sauvegarde des modèles entrainés
output_paths_nb = [NB_FAM_CLF, NB_DIAL_CLF]
output_paths_svm = [SVM_FAM_CLF, SVM_DIAL_CLF]
output_paths_dummy = [DUMMY_FAM, DUMMY_DIAL]
output_paths = [output_paths_nb, output_paths_svm, output_paths_dummy]

# Liste des paramètres à optimiser (à modifier manuellement dans le code)
optimizing_params = {
    'preprocess__featureunion__word_ngrams__wvect__min_df': (0.06, 0.05, 0.1),
    'preprocess__featureunion__char_ngrams__cvect__min_df': (0.01, 0.005, 0),
    'preprocess__featureunion__word_ngrams__wvect__ngram_range': [(1, 1), (1, 2)],
    'preprocess__featureunion__char_ngrams__cvect__ngram_range': [(1, 4), (1, 5), (2, 4), (2, 5)],
}

# Listes des paramètres pour la phase de prétraitement
svm_fam_params = {
    "w_min_df": 0.06,  # Optimized value = 0.06
    "w_ngram_range": (1, 1),  # Optimized value = (1, 1)
    "c_min_df": 0,  # Optimized value = 0
    "c_ngram_range": (1, 5),  # Optimized value = (1, 5)
}
svm_dialect_params = {
    "w_min_df": 0.01,  # Optimized value = 0.01
    "w_ngram_range": (1, 1),  # Optimized value = (1, 1)
    "c_min_df": 0.005,  # Optimized value = 0.005
    "c_ngram_range": (1, 5),  # Optimized value = (1, 5)
}
nb_fam_params = {
    "w_min_df": 0.05,  # Optimized value = 0.05
    "w_ngram_range": (1, 1),  # Optimized value = (1, 1)
    "c_min_df": 0.005,  # Optimized value = 0.005
    "c_ngram_range": (1, 5),  # Optimized value = (1, 5)
}
nb_dialect_params = {
    "w_min_df": 0.05,  # Optimized value = 0.05
    "w_ngram_range": (1, 1),  # Optimized value = (1, 1)
    "c_min_df": 0.005,  # Optimized value = 0.005
    "c_ngram_range": (2, 5),  # Optimized value = (2, 5)
}
dummy_params = {
    "w_min_df": 0.1,
    "w_ngram_range": (1, 1),
    "c_min_df": 0.1,
    "c_ngram_range": (2, 4),
}


if __name__ == '__main__':
    optimizing = get_optimizing_command()

    models.append(DummyClassifier())
    model_names.append("Dummy Classifier")

    model_nb = get_model_command(model_names)  # Retourne 0 (= NB) ou 1 (= SVM) ou 2 (= Dummy)
    model_name = model_names[model_nb]
    model_to_train = models[model_nb]

    command = get_data_command(model_name)

    # Entrainement du modèle pour classifier les grandes familles de dialectes
    if command == 1 or command == 3:
        if optimizing:
            optimization_results = optimizing_clf_params(family_vars['X'], family_vars['y'], optimizing_params, model_to_train)
            save_report(optimization_results, "Optimization_report_fam_{}".format(model_name))
        else:
            output_path = output_paths[model_nb][0]
            if model_nb == 0:
                preprocessing_params = nb_fam_params
            elif model_nb == 1:
                preprocessing_params = svm_fam_params
            else:
                preprocessing_params = dummy_params
            make_simple_clf(output_path=output_path,
                            classifier_to_train=model_to_train, model_name=model_name,
                            family_data=True, prep_params=preprocessing_params)

    # Entrainement du modèle pour classifier les variétés géographiquement plus localisées (dialectes)
    if command == 2 or command == 3:
        if optimizing:
            optimization_results = optimizing_clf_params(dialect_vars['X'], dialect_vars['y'], optimizing_params, model_to_train)
            save_report(optimization_results, "Optimization_report_dial_{}".format(model_name))
        else:
            output_path = output_paths[model_nb][1]
            if model_nb == 0:
                preprocessing_params = nb_dialect_params
            elif model_nb == 1:
                preprocessing_params = svm_dialect_params
            else:
                preprocessing_params = dummy_params
            make_simple_clf(output_path=output_path,
                            classifier_to_train=model_to_train, model_name=model_name,
                            family_data=False, prep_params=preprocessing_params)

