"""Program for training Ensemble 1 classifiers.

This project has been developed by Oriane Nédey and is under licence CC BY-NC-SA 4.0 (https://creativecommons.org/licenses/by-nc-sa/4.0/)
Creative Commons Licence : Attribution - Non Commercial - Share Alike 4.0 International
This license lets others remix, tweak, and build upon your work non-commercially, as long as they credit you and license their new creations under the identical terms.
"""

from training.training_helpers import *

# On prépare les données initiales
from training.training_init import family_vars, dialect_vars, models, model_names

from training.models_manager import save_clf, get_optimizing_command, get_model_command, get_data_command, save_report


# Fonction pour entrainer l'ensemble
def make_clf_e1(output_path, classifier_to_train, model_name, family_data, prep_params, optimizing_params=None):

    if family_data is True:
        vars_dict = family_vars

    else:
        vars_dict = dialect_vars

    preprocess_pipeline, words_pipeline, chars_pipeline = make_preprocess_pipeline(w_min_df=prep_params['w_min_df'],
                                                                                   w_ngram_range=prep_params['w_ngram_range'],
                                                                                   c_min_df=prep_params['c_min_df'],
                                                                                   c_ngram_range=prep_params['c_ngram_range'])

    X_train, X_test, y_train, y_test = train_test_split(vars_dict['X'], vars_dict['y'], test_size=0.2)
    X_test, y_test = add_unknown_data_to_test(X_test, y_test, vars_dict['X_test_unknown'], vars_dict['y_test_unknown'])

    clf1 = Pipeline([("wc_ngrams", preprocess_pipeline), ("model", classifier_to_train)])
    clf2 = Pipeline([("w_ngrams", words_pipeline), ("model", classifier_to_train)])
    clf3 = Pipeline([("c_ngrams", chars_pipeline), ("model", classifier_to_train)])

    ensemble_clf = VotingClassifier(estimators=[('words_chars', clf1), ('words', clf2), ('chars', clf3)], voting='soft')

    if optimizing_params is None:
        ensemble_clf.fit(X_train, y_train)
        evaluate_model(ensemble_clf, X_test, y_test,
                       clf_name="Ensemble 1 - {} ({})".format(model_name, vars_dict['data_desc']),
                       params=prep_params,
                       proba_threshold=vars_dict['threshold'])
        save_clf(ensemble_clf, output_path)
    else:
        # print(ensemble_clf.get_params().keys())
        optimization_results = optimizing_clf_params(vars_dict['X'], vars_dict['y'],
                                                     optimizing_params, ensemble_clf, existing_pipeline=True)
        save_report(optimization_results, "Optimization_report_{}_e1_{}".format(vars_dict['datatype'], model_name))

# Listes des chemins de sortie pour la sauvegarde des modèles entrainés
output_paths_nb = [E1_NB_FAM_CLF, E1_NB_DIAL_CLF]
output_paths_svm = [E1_SVM_FAM_CLF, E1_SVM_DIAL_CLF]
output_paths = [output_paths_nb, output_paths_svm]

# Liste des paramètres à optimiser (à modifier manuellement dans le code)
optimizing_params = {
    # 'words_chars__wc_ngrams__featureunion__char_ngrams__cvect__min_df': (0.01, 0.005, 0),
    # 'words_chars__wc_ngrams__featureunion__char_ngrams__cvect__ngram_range': [(1, 4), (1, 5), (2, 4), (2, 5)],
    # 'words_chars__wc_ngrams__featureunion__word_ngrams__wvect__min_df': (0.05, 0.01),
    # 'words_chars__wc_ngrams__featureunion__word_ngrams__wvect__ngram_range': [(1, 1), (1, 2), (1, 3)],
    #
    # 'chars__c_ngrams__cvect__min_df': (0.01, 0.005, 0),
    # 'chars__c_ngrams__cvect__ngram_range': [(1, 4), (1, 5), (2, 4), (2, 5)],

    'words__w_ngrams__wvect__min_df': (0.05, 0.01),
    'words__w_ngrams__wvect__ngram_range': [(1, 1), (1, 2), (1, 3)],
}

# Listes des paramètres pour la phase de prétraitement
svm_fam_params = {
    "w_min_df": 0.01,  # Optimized value = 0.01 for clf words_chars / 0.05 for clf words
    "w_ngram_range": (1, 1),  # Optimized value = (1, 1) for clf words_chars and for clf words
    "c_min_df": 0.001,  # Optimized value = 0.005 for clf words_chars / 0 for clf chars
    "c_ngram_range": (1, 5),  # Optimized value = (1, 4) for clf words_chars / (1, 5) for clf chars
}
svm_dialect_params = {
    "w_min_df": 0.01,  # Optimized value = 0.01 for clf words_chars / 0.05 for clf words
    "w_ngram_range": (1, 1),  # Optimized value = (1, 1) for clf words_chars and for clf words
    "c_min_df": 0,  # Optimized value = 0 for clf words_chars and for clf chars
    "c_ngram_range": (1, 5),  # Optimized value = (1, 5) for clf words_chars and for clf chars
}
nb_fam_params = {
    "w_min_df": 0.01,  # Optimized value = 0.01 for clf words_chars and for clf words
    "w_ngram_range": (1, 3),  # Optimized value = (1, 1) for clf words_chars / (1, 3) for clf words
    "c_min_df": 0.005,  # Optimized value = 0 for clf words_chars / 0.005 for clf chars
    "c_ngram_range": (2, 4),  # Optimized value = (2, 5) for clf chars / (2, 3) for clf words_chars
}
nb_dialect_params = {
    "w_min_df": 0.01,  # Optimized value = 0.01 for clf words_chars and for clf words
    "w_ngram_range": (1, 2),  # Optimized value = (1, 3) for clf words_chars / (1, 1) for clf words
    "c_min_df": 0.005,  # Optimized value = 0.005 for clf words_chars and for clf chars
    "c_ngram_range": (2, 4),  # Optimized value = (2, 4) for clf words_chars and for clf chars
}


if __name__ == '__main__':
    print("""Vous allez entrainer l'ensemble 1 de classifieurs, qui comportent 3 classifieurs intermédaires sur le modèle de Twist Bytes : 
    - le premier prend en compte les n-grams de mots et de caractères,
    - le deuxième prend en compte uniquement les n-grams de mots,
    - et le troisième prend en compte uniquement les n-grams de caractères.\n""")

    optimizing = get_optimizing_command()
    if optimizing:
        optimizing = optimizing_params
    else:
        optimizing = None

    model_nb = get_model_command(model_names)  # Retourne 0 ou 1
    model_name = model_names[model_nb]
    model_to_train = models[model_nb]

    command = get_data_command(model_name)

    # Entrainement du modèle pour classifier les grandes familles de dialectes
    if command == 1 or command == 3:
        output_path = output_paths[model_nb][0]
        if model_nb == 0:
            preprocessing_params = nb_fam_params
        else:
            preprocessing_params = svm_fam_params
        make_clf_e1(output_path=output_path,
                    classifier_to_train=model_to_train, model_name=model_name,
                    family_data=True, prep_params=preprocessing_params,
                    optimizing_params=optimizing)

    # Entrainement du modèle pour classifier les variétés géographiquement plus localisées (dialectes)
    if command == 2 or command == 3:
        output_path = output_paths[model_nb][1]
        if model_nb == 0:
            preprocessing_params = nb_dialect_params
        else:
            preprocessing_params = svm_dialect_params
        make_clf_e1(output_path=output_path,
                    classifier_to_train=model_to_train, model_name=model_name,
                    family_data=False, prep_params=preprocessing_params,
                    optimizing_params=optimizing)
