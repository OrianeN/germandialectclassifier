"""Contains functions to choose and save classifiers.

This project has been developed by Oriane Nédey and is under licence CC BY-NC-SA 4.0 (https://creativecommons.org/licenses/by-nc-sa/4.0/)
Creative Commons Licence : Attribution - Non Commercial - Share Alike 4.0 International
This license lets others remix, tweak, and build upon your work non-commercially, as long as they credit you and license their new creations under the identical terms.
"""

import pickle

from _constants import OPTIM_FOLDER

def save_clf(clf, output_path):
    # Sauvegarde du modèle
    with open(output_path, "wb") as f:
        my_pickler = pickle.Pickler(f)
        my_pickler.dump(clf)
    print("Modèle sauvegardé !")


def save_report(report, title):
    output_path = OPTIM_FOLDER + title + ".txt"
    with open(output_path, "w", encoding="utf-8") as f:
        f.write(report)
    print("Rapport sauvegardé !")


def get_optimizing_command():
    """
    Demande si on veut entrainer le modèle ou optimiser les paramètres.
    :return: bool True signi
    """
    input_msg = "Souhaitez-vous entrainer un classifieur (E) ou en optimiser les paramètres (O) ?\n"
    optimizing = False
    cmd_ok = False
    while cmd_ok is False:
        try:
            command = input(input_msg)
            assert command in ["E", "e", "O", "o", "0"]
        except TypeError:
            print("Vous devez saisir 'E' si vous voulez entrainer un classifieur, ou 'O' si vous voulez optimiser les paramètres")
            cmd_ok = False
        except AssertionError:
            print("Vous devez saisir 'E' si vous voulez entrainer un classifieur, ou 'O' si vous voulez optimiser les paramètres")
            cmd_ok = False
        else:
            cmd_ok = True
            if command in ["O", "o", "0"]:
                optimizing = True
    return optimizing


def get_model_command(model_names):
    """

    :param model_names: list of strings
    :return: int
    """
    model_quantity = len(model_names)
    input_msg = "Quel modèle souhaitez-vous utiliser pour l'apprentissage ? " \
                "(saisir un nombre entre 1 et {})\n".format(model_quantity)
    for i, name in enumerate(model_names):
        input_msg += "\t{} - {}\n".format(i + 1, name)

    model_nb = 1  # Par défaut, on entraine le modèle pour les familles et le modèle pour les dialectes plus précis
    cmd_ok = False
    while cmd_ok is False:
        try:
            model_nb = int(input(input_msg))
            assert 1 <= model_nb <= model_quantity
        except TypeError:
            print("Vous devez saisir un nombre entier entre 1 et {}".format(model_quantity))
            cmd_ok = False
        except AssertionError:
            print("Vous devez saisir un nombre entier entre 1 et {}".format(model_quantity))
            cmd_ok = False
        else:
            cmd_ok = True
    return model_nb-1


def get_data_command(clf_name):
    command = 3  # Par défaut, on entraine le modèle pour les familles et le modèle pour les dialectes plus précis
    cmd_ok = False
    input_msg = """Quel(s) modèle(s) souhaitez-vous entrainer avec {} ? (saisir 1, 2 ou 3)
    \t1 - Modèle pour classifier les grandes familles de dialectes (Niederdeutsch, Mitteldeutsch, Oberdeutsch)
    \t2 - Modèle pour classifier les dialectes (variétés plus locales telles que Kölsch, Alemannisch, Berlinisch)
    \t3 - Les 2 modèles : classification en famille puis variété plus précise géographiquement\n""".format(clf_name)
    while cmd_ok is False:
        try:
            command = int(input(input_msg))
            assert command in [1, 2, 3]
        except TypeError:
            print("Vous devez saisir un nombre (1, 2 ou 3)")
            cmd_ok = False
        except AssertionError:
            print("Vous devez saisir au choix : 1, 2 ou 3 !")
            cmd_ok = False
        else:
            cmd_ok = True
            print("\nC'est parti !\n")
    return command
