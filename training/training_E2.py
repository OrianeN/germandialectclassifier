"""Program for training Ensemble 2 classifiers.

This project has been developed by Oriane Nédey and is under licence CC BY-NC-SA 4.0 (https://creativecommons.org/licenses/by-nc-sa/4.0/)
Creative Commons Licence : Attribution - Non Commercial - Share Alike 4.0 International
This license lets others remix, tweak, and build upon your work non-commercially, as long as they credit you and license their new creations under the identical terms.
"""

from training.training_helpers import *

# On prépare les données initiales
from training.training_init import family_vars, dialect_vars, models, model_names

from training.models_manager import save_clf, get_optimizing_command, get_model_command, get_data_command, save_report


# Fonction pour entrainer l'ensemble
def make_clf_e2(output_path, model, model_name, family_data, optimizing_params=None):

    if family_data is True:
        vars_dict = family_vars

    else:
        vars_dict = dialect_vars


    X_train, X_test, y_train, y_test = train_test_split(vars_dict['X'], vars_dict['y'], test_size=0.2)
    X_test, y_test = add_unknown_data_to_test(X_test, y_test, vars_dict['X_test_unknown'], vars_dict['y_test_unknown'])

    words_clfs = []
    n = 1
    while n < 4:
        clf_preprocess = make_words_pipeline(ngram_range=(n, n), min_df=0)  # Optimized value : 0 for nb fam / dial (n=2) and svm fam / dial (n=2)
        clf = Pipeline([("preprocess", clf_preprocess),
                        ("model", model)])
        clf_name = "clf_w{}".format(n)
        words_clfs.append((clf_name, clf))
        n += 1

    chars_clfs = []
    n = 1
    while n < 8:
        clf_preprocess = make_chars_pipeline(ngram_range=(n, n), min_df=0)  # Optimized value : 0 for nb fam and svm dial (n= 2) / 0.005 for nb dial (n=2) / 0.001 for svm fam (n=2)
        clf = Pipeline([("preprocess", clf_preprocess),
                        ("svm", model)])
        clf_name = "clf_c{}".format(n)
        chars_clfs.append((clf_name, clf))
        n += 1

    clfs = words_clfs + chars_clfs

    ensemble_clf = VotingClassifier(estimators=clfs, voting='soft')

    if optimizing_params is None:
        ensemble_clf.fit(X_train, y_train)
        evaluate_model(ensemble_clf, X_test, y_test,
                       clf_name="Ensemble 2 - {} ({})".format(model_name, vars_dict['data_desc']),
                       proba_threshold=vars_dict['threshold'])
        save_clf(ensemble_clf, output_path)
    else:
        optimization_results = optimizing_clf_params(vars_dict['X'], vars_dict['y'],
                                                     optimizing_params, ensemble_clf, existing_pipeline=True)
        save_report(optimization_results, "Optimization_report_{}_e2_{}".format(vars_dict['datatype'], model_name))


output_paths_nb = [E2_NB_FAM_CLF, E2_NB_DIAL_CLF]
output_paths_svm = [E2_SVM_FAM_CLF, E2_SVM_DIAL_CLF]
output_paths = [output_paths_nb, output_paths_svm]

optimizing_params = {  # Optimisation partielle car on ne change pas le min_df pour chaque classifier
    # 'clf_w1__preprocess__wvect__min_df': (0.1, 0.05, 0.01, 0),
    'clf_w2__preprocess__wvect__min_df': (0.005, 0.001, 0),
    # 'clf_w3__preprocess__wvect__min_df': (0.1, 0.05, 0.01),

    # 'clf_c1__preprocess__cvect__min_df': (0.1, 0.05, 0.01, 0.001),
    'clf_c2__preprocess__cvect__min_df': (0.01, 0.005, 0.001, 0),
    # 'clf_c3__preprocess__cvect__min_df': (0.1, 0.05, 0.01, 0.001),
    # 'clf_c4__preprocess__cvect__min_df': (0.1, 0.05, 0.01),
    # 'clf_c5__preprocess__cvect__min_df': (0.1, 0.05, 0.01),
    # 'clf_c6__preprocess__cvect__min_df': (0.1, 0.05, 0.01),
    # 'clf_c7__preprocess__cvect__min_df': (0.1, 0.05, 0.01),
}

if __name__ == '__main__':
    print("""Vous allez entrainer l'ensemble 2 de classifieurs, qui comportent de multiples classifieurs intermédaires sur le modèle MAZA : 
    - une 1ere série de classifieurs qui prennent en comptent uniquement des n-grams de caractères, n ne prenant à chaque fois qu'une seule valeur,
    - une 2eme série de classifieurs qui prennent en comptent uniquement des n-grams de mots, n ne prenant à chaque fois qu'une seule valeur.\n""")

    optimizing = get_optimizing_command()
    if optimizing:
        optimizing = optimizing_params
    else:
        optimizing = None

    model_nb = get_model_command(model_names)  # Renvoie 0 ou 1
    model_name = model_names[model_nb]
    model_to_train = models[model_nb]

    command = get_data_command(model_name)

    # Entrainement du modèle pour classifier les grandes familles de dialectes
    if command == 1 or command == 3:
        output_path = output_paths[model_nb][0]
        make_clf_e2(output_path=output_path,
                    model=model_to_train, model_name=model_name,
                    family_data=True, optimizing_params=optimizing)

    # Entrainement du modèle pour classifier les variétés géographiquement plus localisées (dialectes)
    if command == 2 or command == 3:
        output_path = output_paths[model_nb][1]
        make_clf_e2(output_path=output_path,
                    model=model_to_train, model_name=model_name,
                    family_data=False, optimizing_params=optimizing)
