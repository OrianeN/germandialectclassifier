"""Contains useful functions for training this project's classifiers.

This project has been developed by Oriane Nédey and is under licence CC BY-NC-SA 4.0 (https://creativecommons.org/licenses/by-nc-sa/4.0/)
Creative Commons Licence : Attribution - Non Commercial - Share Alike 4.0 International
This license lets others remix, tweak, and build upon your work non-commercially, as long as they credit you and license their new creations under the identical terms.
"""

import time
import re

from _constants import *
from predict_helpers import predict_with_threshold


def create_df_from_csv(csv_path):
    """
    Create DataFrame (pandas) from a well-formed csv file
    :param csv_path: string file_path
    :return: DataFrame object
    """
    df = pd.read_csv(csv_path,
                     encoding='utf-8',
                     delimiter='\t',
                     header=None,
                     names=['dialect_family', 'dialect', 'sentence'],
                     dtype={'dialect_family': 'category',
                            'dialect': 'category',
                            'sentence': 'object'})
    return df


# Creation of Pipelines for the Preprocessing phase

class SingleColumnSelector(BaseEstimator, TransformerMixin):
    """This class is used for selecting a single column in a DataFrame during a preprocessing transformation."""
    def __init__(self, key):
        self.key = key

    def fit(self, X, y=None):
        return self

    def transform(self, data_dict):
        return data_dict[self.key]


def split_into_tokens_wo_punct_nltk(sentence):
    """
    Splits a string into tokens using NLTK + removes its punctuation.
    Does not return sentences containing less than 4 words.
    :param sentence: string
    :return: list of all tokens in the sentence without punctuation
    """
    words_with_punct = word_tokenize(sentence)
    words_wo_punct = [re.sub('([^a-zA-Z])', '', w) for w in words_with_punct]
    words_not_empty = [w for w in words_wo_punct if w != '']
    if len(words_not_empty) <= 3:
        return []
    return words_not_empty


def make_words_pipeline(min_df, ngram_range, tokenizer=split_into_tokens_wo_punct_nltk, lowercase=True):
    """
    Creates the pipeline that vectorizes the sentences into word n-grams
    :param min_df: float. minimum document frequency
    :param ngram_range: tuple (int, int). the range of n for all word n-grams
    :param tokenizer: function. to tokenize each sentence into words (default nltk word_tokenize + removes punctuation)
    :param lowercase: boolean. turns all sentences to lowercase if True (default)
    :return: Pipeline object
    """
    # Creation of the TfidfVectorizer transformer (= CountVectorizer + TfidfTransformer)
    word_vectorizer = TfidfVectorizer(tokenizer=tokenizer,
                                      lowercase=lowercase,
                                      min_df=min_df,
                                      ngram_range=ngram_range,
                                      analyzer='word')

    words_pipeline = Pipeline([("cselect", SingleColumnSelector(key='sentence')),
                               ("wvect", word_vectorizer)])
    return words_pipeline


def make_chars_pipeline(min_df, ngram_range, lowercase=True):
    """
    Creates the pipeline that vectorizes the sentences into character ngrams (inside of word boundaries)
    :param min_df: float. minimum document frequency
    :param ngram_range: tuple (int, int). the range of n for all character n-grams
    :param lowercase: boolean. turns all sentences to lowercase if True (default)
    :return: Pipeline object
    """
    # Creation of the TfidfVectorizer transformer (= CountVectorizer + TfidfTransformer)
    char_vectorizer = TfidfVectorizer(lowercase=lowercase,
                                      min_df=min_df,
                                      ngram_range=ngram_range,
                                      analyzer='char_wb'
                                      )
    chars_pipeline = Pipeline([("cselect", SingleColumnSelector(key='sentence')),
                               ("cvect", char_vectorizer)])
    return chars_pipeline


def make_preprocess_pipeline(w_min_df=0.1, w_ngram_range=(1, 2),
                             c_min_df=0.01, c_ngram_range=(2, 5)):
    """
    Creates a preprocess pipeline with specific parameters.
    :param w_min_df: float. minimum document frequency for word n-grams
    :param w_ngram_range: tuple (int, int). the range of n for all word n-grams
    :param c_min_df: float. minimum document frequency for character n-grams
    :param c_ngram_range: tuple (int, int). the range of n for all character n-grams
    :return: list : 0 = the entire preprocessing pipeline.
                    1 = pipeline for transforming sentences into word n-grams.
                    2 = pipeline for transforming sentences into character n-grams
    """
    words_pipeline = make_words_pipeline(tokenizer=split_into_tokens_wo_punct_nltk, lowercase=True,
                                         min_df=w_min_df, ngram_range=w_ngram_range)

    chars_pipeline = make_chars_pipeline(lowercase=True, min_df=c_min_df,
                                         ngram_range=c_ngram_range)

    union = FeatureUnion(transformer_list=[("word_ngrams", words_pipeline),
                                           ("char_ngrams", chars_pipeline)])

    preprocess_pipeline = make_pipeline(union)
    return preprocess_pipeline, words_pipeline, chars_pipeline


# Function for optimizing the classifier parameters
def optimizing_clf_params(X, y, parameters_to_optimize, classifier_to_train, cv=3, existing_pipeline=False):
    """
    This function calculates the best parameter combinations
    and returns an optimizing report
    :param X: DataFrame. contains the features for classification
    (sentences or sentences+families)
    :param y: DataFrame. contains the labels to classify (families or dialects)
    :param parameters_to_optimize: dictionary of parameters names and their values
    to test in tuples (according to sklearn...GridSearchCV() norms)
    :param classifier_to_train: estimator that implements fit() and predict()
    :param cv: int. number of folds for cross-validation
    :param existing_pipeline:
    Pipeline that includes the preprocessing and the classifying steps.
    If False (default), a Pipeline will be created with the classifier_to_train and
    the default parameters of make_preprocess_pipeline()
    :return: string. The optimization report
    """
    # Creates a Pipeline with preprocessing + classifying if it does not already exist
    if existing_pipeline is False:
        preprocess_pipeline, _w, _c = make_preprocess_pipeline()
        classifier_pipeline = Pipeline([("preprocess", preprocess_pipeline),
                                        ("clf", classifier_to_train)])
    else:
        classifier_pipeline = classifier_to_train

    start_time = time.time()
    optimizing_classifier = GridSearchCV(classifier_pipeline, parameters_to_optimize,
                                         cv=cv, iid=False, return_train_score=True,
                                         n_jobs=-1)
    optimizing_classifier.fit(X, y)
    end_time = time.time()

    # Calculation of the optimizing time
    time_in_seconds = int(end_time - start_time)
    seconds = int(time_in_seconds % 60)
    time_in_minutes = time_in_seconds / 60
    minutes = int(time_in_minutes % 60)
    hours = int(time_in_minutes / 60)

    # Creation of the report
    report = ""
    optimizing_time = "Durée d'optimisation : {} heures, {} minutes et {} secondes".format(hours, minutes, seconds)
    report += optimizing_time + "\n"
    report += "Paramètres évalués :\n"
    for key, value in parameters_to_optimize.items():
        report += "{}: {}\n".format(key, value)
    best_score = "\nBest score : {}".format(optimizing_classifier.best_score_)
    report += best_score + "\n\n"
    for param_name in sorted(parameters_to_optimize.keys()):
        param_results = "%s: %r" % (param_name,
                                    optimizing_classifier.best_params_[param_name])
        report += param_results + "\n"
    optimization_results = "\nDetailed results : {}".format(
        pd.DataFrame(optimizing_classifier.cv_results_))
    report += optimization_results + "\n"
    print(report)
    return report


# Fonctions pour l'évaluation des modèles
def infos_features(words_pipeline, chars_pipeline):
    """
    Prints basic information about the extracted features : number of features + a few examples
    :param words_pipeline: Pipeline object for transforming sentences into word n-grams
    :param chars_pipeline: Pipeline object for transforming sentences into character n-grams
    :return: None
    """
    fnames_words = words_pipeline.steps[1][1].get_feature_names()
    fnames_chars = chars_pipeline.steps[1][1].get_feature_names()
    w_nb = len(fnames_words)
    c_nb = len(fnames_chars)
    w_sample = fnames_words[0:30]
    c_sample = fnames_chars[0:500]

    infos_dict = {
        "w": {'nb': w_nb, 'sample': w_sample},
        "c": {'nb': c_nb, 'sample': c_sample},
    }

    print("Nombre de traits extraits - n-grams de mots : {}".format(w_nb))
    print("Extrait des n-grams de mots extraits : {}".format(w_sample))
    print("Nombre de traits extraits - n-grams de caractères : {}".format(c_nb))
    print("Extrait des n-grams de caractères extraits : {}".format(c_sample))

    return infos_dict


def add_unknown_data_to_test(X_test, y_test, X_test_unknown, y_test_unknown):
    """
    Add unknown data to the test_set in order to evaluate the threshold value
    :param X_test: DataFrame with dialect sentences (and potentially family labels)
    :param y_test: DataFrame with dialect or family labels
    :param X_test_unknown: DataFrame with unknown sentences (and potentially 'unknown' label)
    :param y_test_unknown: DataFrame with 'unknown' label
    :return: list of 2 DataFrame that include unknown data : X_test and y_test
    """
    X_test = pd.concat([X_test, X_test_unknown], ignore_index=True)
    X_test = X_test.fillna(0.00)
    y_test = pd.concat([y_test, y_test_unknown], ignore_index=True, sort=True)
    return X_test, y_test


def evaluate_model(trained_model, X_test, y_test, clf_name,
                   params=None, features_sample=None, proba_threshold=None):
    """
    Function for evaluating a trained classifier.
    :param trained_model: Pipeline object. Trained classifier to evaluate.
    :param X_test: DataFrame for testing (to extract the features)
    :param y_test: DataFrame for testing (labels that should be predicted)
    :param clf_name: name of the classifier for the plots
    :param params: dict of parameters for the preprocessing step
    (see function make_preprocess_pipeline). Default None.
    :param features_sample: dict with infos concerning the features
    (returned by function infos_features). Default None
    :param proba_threshold: float (value between 0 and 1). threshold value.
    :return: None but creates plots that are saved as PNG files + a training report.
    """
    if proba_threshold:
        y_pred = predict_with_threshold(trained_model, X_test, proba_threshold)
    else:
        y_pred = trained_model.predict(X_test)

    report_title = "Training report - Classifier {}".format(clf_name)
    accuracy_report = "Accuracy : {}".format(accuracy_score(y_test, y_pred))
    confusion_matrix_report = "Confusion matrix :\n{}\n(row=expected, col=predicted)".format(confusion_matrix(y_test, y_pred))
    if params is not None:
        parameters = "Parameters :\n"
        for key, value in params.items():
            parameters += "{}: {}\n".format(key, value)
    else:
        parameters = ""

    if features_sample is not None:
        features = "Features sample - words ({}) : {}\n".format(
            features_sample["w"]["nb"], features_sample["w"]["sample"])
        features += "Features sample - character n-grams ({}) : {}\n".format(
            features_sample['c']['nb'], features_sample['c']['sample'])
    else:
        features = ""

    with open(PLOTS + report_title + ".txt", "w", encoding='utf-8') as f:
        f.write("{title}\n\n{parameters}\n{features}\n{accuracy}\n\n{matrix}\n\n{classification}".format(
            title=report_title,
            parameters=parameters,
            features=features,
            accuracy=accuracy_report,
            matrix=confusion_matrix_report,
            classification=classification_report(y_test, y_pred)))

    print(accuracy_report)
    print(confusion_matrix_report)
    print(classification_report(y_test, y_pred))

    plot_title = "Matrice de confusion"
    if clf_name:
        plot_title += " - " + clf_name
    normalized_title = plot_title + " - " + "normalisé"

    plt_confusion_matrix(y_test, y_pred, title=plot_title)
    plt_confusion_matrix(y_test, y_pred, normalize=True, title=normalized_title)


# Fonctions pour la création des graphiques
def plot_dialects_df(df):
    """
    Show basic statistics corresponding to the DataFrame :
    number of sentences for each dialect and each dialect family.
    :param df: DataFrame object
    :return: None but saves the created plots as PNG files
    """
    df.describe()
    print(df.head())
    print(df.info())

    # Graph : Number of sentences for each dialect family
    save_path = PLOTS + 'stats-corpus-familles.png'
    df.groupby('dialect_family').size().plot(kind='bar')
    plt.title("Nombre de phrases du corpus par famille de dialectes")
    plt.ylabel('Nombre de phrases')
    plt.xlabel('Nom de la famille de dialecte')
    plt.xticks(rotation='horizontal')
    plt.savefig(save_path)
    plt.show()

    # Graph : Number of sentences for each dialect
    save_path = PLOTS + 'stats-corpus-dialectes.png'
    df.groupby('dialect').size().plot(kind='bar')
    plt.title("Nombre de phrases du corpus par dialecte")
    plt.ylabel('Nombre de phrases')
    plt.xlabel('Nom du dialecte')
    plt.xticks(rotation='horizontal')
    plt.savefig(save_path)
    plt.show()

def plt_confusion_matrix(y_test, y_pred, normalize=False, title="Confusion matrix"):
    """
    Plots a nice confusion matrix.
    :param y_test: list of predicted labels
    :param y_pred: list of labels that should have been predicted.
    :param normalize: boolean. If False, the plots shows the number of sentences predicted.
    If True, shows the percentage of sentences predicted.
    :param title: string. Title of the plot.
    :return: Nothing but saves the plot as a PNG file and shows it.
    """
    labels = list(set(y_pred))
    cm = confusion_matrix(y_test, y_pred, labels)
    fig = plt.figure()
    ax = fig.add_subplot(111)

    cax = ax.matshow(cm, cmap=plt.cm.binary, interpolation='nearest')
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    fig.suptitle(title, fontsize=14, wrap=True)
    fig.colorbar(cax)
    ax.set_xlabel('Predicted')
    ax.set_ylabel('True')
    ax.xaxis.set_ticklabels([''] + labels, rotation=45)
    ax.yaxis.set_ticklabels([''] + labels)

    plt.subplots_adjust(hspace=0.6)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 1.5 if normalize else cm.max() / 2
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")

    plt.savefig(PLOTS + title)
    plt.show()
