"""Program for training Tree classifiers.

This project has been developed by Oriane Nédey and is under licence CC BY-NC-SA 4.0 (https://creativecommons.org/licenses/by-nc-sa/4.0/)
Creative Commons Licence : Attribution - Non Commercial - Share Alike 4.0 International
This license lets others remix, tweak, and build upon your work non-commercially, as long as they credit you and license their new creations under the identical terms.
"""

from sklearn import tree
import graphviz

from _constants import PLOTS

from training.training_helpers import *

# On prépare les données initiales
from training.training_init import family_vars, dialect_vars

from training.models_manager import save_clf, get_optimizing_command, get_model_command, get_data_command, save_report


# Fonction pour entrainer les classifieurs simples avec MultinomialNB ou SVM
def make_tree(output_path, family_data):

    classifier_to_train = tree.DecisionTreeClassifier()
    model_name = "Tree"

    if family_data is True:
        vars_dict = family_vars
        w_min_df = 0.1
        w_ngram_range = (1, 1)
        c_min_df = 0.1
        c_ngram_range = (2, 4)

    else:
        vars_dict = dialect_vars
        w_min_df = 0.1
        w_ngram_range = (1, 1)
        c_min_df = 0.1
        c_ngram_range = (2, 4)

    preprocess_pipeline, words_pipeline, chars_pipeline = make_preprocess_pipeline(w_min_df=w_min_df,
                                                                                   w_ngram_range=w_ngram_range,
                                                                                   c_min_df=c_min_df,
                                                                                   c_ngram_range=c_ngram_range)

    X_train, X_test, y_train, y_test = train_test_split(vars_dict['X'], vars_dict['y'], test_size=0.2)
    X_test, y_test = add_unknown_data_to_test(X_test, y_test,
                                              vars_dict['X_test_unknown'], vars_dict['y_test_unknown'])

    # Chaine de prétraitement + apprentissage
    classifier_pipeline = Pipeline([("preprocess", preprocess_pipeline),
                                    ("clf", classifier_to_train)])

    # Apprentissage sur les données d'entrainement
    classifier_pipeline.fit(X_train, y_train)

    # Affichage des stats concernant les features extraites
    infos_features(words_pipeline, chars_pipeline)

    # Evaluation complète du modèle entrainé
    clf_name = "{} ({})".format(model_name, vars_dict['data_desc'])
    evaluate_model(classifier_pipeline, X_test, y_test, clf_name, proba_threshold=vars_dict['threshold'])

    save_clf(classifier_pipeline, output_path)

    build_tree = classifier_pipeline.named_steps['clf']

    # Récupération des features les plus importantes
    ft_importances = build_tree.feature_importances_
    print(ft_importances)

    # Création du graphe
    dot_data = tree.export_graphviz(classifier_pipeline.named_steps['clf'], out_file=None)
    graph = graphviz.Source(dot_data)
    graph.render(PLOTS + "DecisionTree_{}".format(vars_dict['datatype']))


output_paths = [CLASSIFIERS + "saved_clf_tree_fam", CLASSIFIERS + "saved_clf_tree_dial"]


if __name__ == '__main__':

    command = get_data_command("Arbre de décision")

    # Entrainement du modèle pour classifier les grandes familles de dialectes
    if command == 1 or command == 3:
        output_path = output_paths[0]
        make_tree(output_path=output_path, family_data=True)

    # Entrainement du modèle pour classifier les variétés géographiquement plus localisées (dialectes)
    if command == 2 or command == 3:
        output_path = output_paths[1]
        make_tree(output_path=output_path, family_data=False)

