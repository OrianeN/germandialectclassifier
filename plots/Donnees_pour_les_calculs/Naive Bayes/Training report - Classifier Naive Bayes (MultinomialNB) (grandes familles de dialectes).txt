Training report - Classifier Naive Bayes (MultinomialNB) (grandes familles de dialectes)

Parameters :
w_min_df: 0.05
w_ngram_range: (1, 1)
c_min_df: 0.005
c_ngram_range: (1, 5)

Features sample - words (17) : ['a', 'an', 'd', 'da', 'dat', 'de', 'der', 'die', 'im', 'in', 'is', 'mit', 'se', 'so', 'un', 'und', 'vo']
Features sample - character n-grams (4351) : [' ', ' (', ' ( ', ' ()', ' () ', ' )', ' ) ', ' -', ' - ', ' .', ' . ', ' [', ' [ ', ' []', ' [] ', ' ]', ' ] ', ' a', ' a ', ' aa', ' aa ', ' ab', ' aba', ' aba ', ' abe', ' aber', ' ac', ' ach', ' acht', ' af', ' ah', ' al', ' al ', ' all', ' all ', ' alle', ' als', ' als ', ' alt', ' alte', ' am', ' am ', ' an', ' an ', ' and', ' ande', ' ann', ' anne', ' ans', ' ant', ' ap', ' ar', ' arb', ' as', ' as ', ' au', ' au ', ' auf', ' auf ', ' aus', ' aus ', ' aut', ' auto', ' av', ' ave', ' aver', ' aw', ' b', ' ba', ' bai', ' bair', ' bau', ' bay', ' baye', ' be', ' bea', ' bear', ' bed', ' bei', ' bei ', ' beim', ' bek', ' beka', ' bel', ' ben', ' ber', ' berl', ' bes', ' best', ' bet', ' bez', ' bi', ' bi ', ' bie', ' bil', ' bild', ' bin', ' bis', ' bis ', ' bit', ' bl', ' ble', ' bli', ' blo', ' bloo', ' bloß', ' bo', ' boa', ' boar', ' br', ' bra', ' brau', ' bre', ' bri', ' bro', ' bru', ' bs', ' bu', ' bä', ' bü', ' c', ' ca', ' ch', ' cha', ' co', ' d', ' d ', ' da', ' da ', ' daa', ' daar', ' dag', ' dam', ' dan', ' dann', ' das', ' das ', ' dass', ' dat', ' dat ', ' daz', ' dazu', ' de', ' de ', ' dea', ' dee', ' dei', ' deit', ' dem', ' dem ', ' den', ' den ', ' denk', ' denn', ' der', ' der ', ' des', ' des ', ' di', ' di ', ' dia', ' dial', ' dic', ' dich', ' die', ' die ', ' dies', ' dir', ' dis', ' diss', ' dit', ' dit ', ' do', ' do ', ' doc', ' doch', ' dom', ' dor', ' dor ', ' dr', ' dr ', ' dra', ' dre', ' drei', ' dri', ' dro', ' dru', ' du', ' du ', ' dua', ' dur', ' durc', ' dä', ' dä ', ' dè', ' dè ', ' dö', ' dör', ' dü', ' e', ' e ', ' ea', ' eb', ' ec', ' ee', ' een', ' een ', ' eene', ' eer', ' eh', ' ehr', ' ei', ' ein', ' eine', ' el', ' em', ' em ', ' en', ' en ', ' end', ' ene', ' eng', ' ent', ' ents', ' er', ' er ', ' ers', ' erst', ' es', ' es ', ' et', ' et ', ' eu', ' eur', ' euro', ' f', ' fa', ' fas', ' fe', ' fei', ' fer', ' fes', ' fi', ' fia', ' fia ', ' fin', ' fl', ' fle', ' fo', ' for', ' fr', ' fra', ' fran', ' fre', ' frei', ' fri', ' fro', ' frö', ' frü', ' fu', ' fö', ' för', ' för ', ' fü', ' für', ' für ', ' g', ' ga', ' gan', ' ganz', ' gar', ' ge', ' geb', ' gee', ' geh', ' gei', ' gel', ' gem', ' gen', ' ger', ' ges', ' gf', ' gh', ' gi', ' gib', ' gibt', ' gl', ' gla', ' gle', ' glei', ' gm', ' gn', ' go', ' goo', ' good', ' gr', ' gra', ' gre', ' gri', ' gro', ' gs', ' gsc', ' gsch', ' gsi', ' gu', ' gun', ' gung', ' gw', ' h', ' ha', ' hab', ' hal', ' ham', ' ham ', ' han', ' han ', ' hand', ' har', ' harr', ' hat', ' hat ', ' hatt', ' hau', ' he', ' he ', ' heb', ' hebb', ' hee', ' hei', ' heit', ' hen', ' her', ' het', ' het ', ' hett', ' heu', ' heut', ' hi', ' hie', ' hier', ' hin', ' ho', ' hoc', ' hoch', ' hod', ' hod ', ' hol', ' hom', ' hot', ' hot ', ' hu', ' huu', ' huus', ' hä', ' hät', ' hät ', ' hè', ' hö', ' hör', ' hör ', ' hü', ' i', ' i ', ' ic', ' ich', ' ich ', ' ick', ' ick ', ' ih', ' ihr', ' ihr ', ' ihre', ' ik', ' ik ', ' im', ' im ', ' imm', ' imma', ' imme', ' in', ' in ', ' in`', ' ind', ' inn', ' ins', ' int', ' inte', ' ir', ' is', ' is ', ' is.', ' is. ', ' isc', ' isch', ' iss', ' iss ', ' iw', ' j', ' ja', ' ja ', ' jah', ' jahr', ' jan', ' janz', ' je', ' jeb', ' jed', ' jede', ' jeh', ' jem', ' jen', ' jer', ' jes', ' jesc', ' jet', ' jetz', ' ji', ' jib', ' jibt', ' jl', ' jo', ' jo ', ' joa', ' joh', ' johr', ' jr', ' ju', ' jun', ' jung', ' jü', ' k', ' ka', ' kan', ' kann', ' kar', ' ke', ' kee', ' keen', ' kei', ' ken', ' kenn', ' ki', ' kie', ' kin', ' kind', ' kl', ' kla', ' kle', ' klo', ' kn', ' ko', ' koa', ' kom', ' komm', ' kon', ' kop', ' kopp', ' kr', ' kra', ' kre', ' kri', ' krie', ' ku', ' kum', ' kumm', ' kun', ' kunn', ' kö', ' köl', ' köls', ' kön', ' kü', ' l', ' la', ' lan', ' land', ' lang', ' lat', ' lau', ' le', ' leb', ' lee', ' lei', ' let', ' letz', ' li', ' lie', ' lin', ' lo', ' los', ' lu', ' lä', ' lü', ' lüt', ' lütt', ' m', ' ma', ' ma ', ' maa', ' mac']

Accuracy : 0.8793037490436113

Confusion matrix :
[[ 649   88   77   74]
 [  21 2004   75   57]
 [  39   62 1906   76]
 [   1   29   32   38]]
(row=expected, col=predicted)

               precision    recall  f1-score   support

Mitteldeutsch       0.91      0.73      0.81       888
Niederdeutsch       0.92      0.93      0.92      2157
  Oberdeutsch       0.91      0.92      0.91      2083
      unknown       0.16      0.38      0.22       100

     accuracy                           0.88      5228
    macro avg       0.72      0.74      0.72      5228
 weighted avg       0.90      0.88      0.89      5228
