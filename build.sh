#!/bin/sh
echo "GermanDialectClassifier is being installed..."
pip install -r requirements.txt # Installation of the required modules
python nltk_installer.py # Installation of the punkt NLTK library