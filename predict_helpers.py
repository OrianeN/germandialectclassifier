"""Contains threshold constants and the function for predicting dialects.

This project has been developed by Oriane Nédey and is under licence CC BY-NC-SA 4.0 (https://creativecommons.org/licenses/by-nc-sa/4.0/)
Creative Commons Licence : Attribution - Non Commercial - Share Alike 4.0 International
This license lets others remix, tweak, and build upon your work non-commercially, as long as they credit you and license their new creations under the identical terms.
"""

# Threshold values
threshold_fam = 0.5  # Optimized value = 0.5
threshold_dial = 0.3  # Optimized value = 0.3


def predict_with_threshold(clf, X, threshold):
    """
    Predicts classes of X with classifier clf,
    using probabilities with a given threshold value
    :param clf: trained/fitted classifier
    :param X: DataFrame. data used for prediction
    :param threshold: float
    :return: array of predicted labels. shape=[n_samples]
    """
    y_pred_proba = clf.predict_proba(X)
    print(y_pred_proba[0:15])
    results = []
    classes = clf.classes_
    for sample_probas in y_pred_proba:
        max_proba = 0
        result_index = -1
        for i, proba in enumerate(sample_probas):
            if proba >= max_proba and proba >= threshold:
                max_proba = proba
                result_index = i
        if result_index == -1:
            predicted_class = "unknown"
        else:
            predicted_class = classes[result_index]
        results.append(predicted_class)
    return results