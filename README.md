<h1 align="center">Welcome to germandialectclassifier 👋</h1>
<p>
  <img src="https://img.shields.io/badge/version-1.1.0-blue.svg?cacheSeconds=2592000" />
  <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">License: CC BY-NC-SA 4.0</a>
</p>

> Tool for identifying dialects of German in textual resources

## Presentation

GermanDialectClassifier is a Python tool for identifying dialects of German in 
textual resources.

It was developed by Oriane Nédey during her master's project in Language 
Technologies at Strasbourg University.

The master's thesis has been published on ECRIN - the
theses platform of the University of Strasbourg: https://publication-theses.unistra.fr/public/memoires/2019/FLCE/2019_nedey_oriane.pdf

## Components

GermanDialectClassifier has several components :
- Training and inference scripts and modules:
  - folder `training`: scripts and modules for training the classifiers
  - file `predict_helpers.py`: module used for the predicting phase (evaluation or website)
- Models checkpoints: 
  - folder `classifier`: last trained classifiers (python objects)
  - folder `official_classifiers`: trained classifiers, whose parameters and performance results were used for interpretation in the master's thesis
- Reports:
  - folder `optimization_reports`: reports created in the classifers' optimization phase
  - folder `plots` : graphs and performance reports
  - file : `Oriane_NEDEY_Memoire_LIT_2019.pdf`: master's thesis linked to this project
- Demo web app:
  - **folder `predicting`**: website with a summary of the project and a demo app
- Installation and launch:
  - file `nltk_installer.py`: script to install NLTK's punkt library
  - file `requirements.txt`: list of the project's dependencies
  - file `requirements_no_demoai.txt`: patched list of dependencies to run the website
  - **file `build.sh`**: a script to install the app
  - **file `run.sh`**: a script to run the app (local website)

## Install

The list dependencies is available in the file `requirements.txt`. 
However, installation might not be working anymore with these requirements. An alternative is to install `requirements_no_demoai.txt` 
instead to be able to launch the web app (but only the Naïve Bayes model will work).

Clone this repository :

```sh
git clone https://gitlab.com/OrianeN/germandialectclassifier
```

Build the app :

```sh on linux
$ sh build.sh
```
```sh on Windows
(Windows)> build.sh
```

## Usage

To try the app, launch the *run.sh* script

```sh on linux
$ sh run.sh
```
```sh on Windows
(Windows)> run.sh
```

To deploy in production, you have to call predicting/wsgi.py with your favorite
Python WSGI HTTP Server.

Example with [Gunicorn](https://gunicorn.org/) :

```sh
$ cd predicting && gunicorn --bind 0.0.0.0:5000 wsgi:app
```

## Author

[👤 **Oriane Nedey**](https://www.linkedin.com/in/oriane-nedey/)


## Show your support

Give a ⭐️ if this project helped you!

## 📝 License

This project is [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) licensed.

Creative Commons Licence : Attribution - Non Commercial - Share Alike 4.0 International

This license lets others remix, tweak, and build upon your work non-commercially, as long as they credit you and license their new creations under the identical terms.
***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
