Durée d'optimisation : 1 heures, 24 minutes et 35 secondes
Paramètres évalués :
words_chars__wc_ngrams__featureunion__char_ngrams__cvect__ngram_range: [(1, 3), (1, 4), (1, 5), (1, 6), (2, 3), (2, 4), (2, 5), (2, 6)]
chars__c_ngrams__cvect__ngram_range: [(1, 2), (1, 3), (1, 4), (1, 5), (1, 6), (2, 3), (2, 4), (2, 5), (2, 6)]

Best score : 0.8279982967067724

chars__c_ngrams__cvect__ngram_range: (2, 5)
words_chars__wc_ngrams__featureunion__char_ngrams__cvect__ngram_range: (2, 3)

Detailed results :     mean_fit_time  mean_score_time  mean_test_score  mean_train_score  \
0       29.096106        13.979988         0.805457          0.901770   
1       32.485558        15.110882         0.810798          0.897363   
2       35.686303        16.005298         0.807794          0.894574   
3       38.659034        16.645741         0.805376          0.893677   
4       27.007452        13.154792         0.811464          0.908089   
5       30.221276        14.210377         0.818559          0.904793   
6       33.324450        15.050372         0.814191          0.903584   
7       36.063993        15.853114         0.812825          0.902511   
8       31.974029        15.192018         0.816611          0.908927   
9       34.568421        16.351341         0.818442          0.902277   
10      38.002794        17.343380         0.812942          0.899352   
11      41.719583        18.353499         0.809705          0.898065   
12      29.550751        14.252903         0.820511          0.913959   
13      32.504182        15.433813         0.824331          0.909746   
14      35.792005        16.340161         0.820041          0.907114   
15      38.496233        17.070950         0.817193          0.906158   
16      34.540826        16.441739         0.820237          0.909044   
17      37.615251        17.679525         0.815595          0.901029   
18      40.706273        18.543739         0.809705          0.896290   
19      43.704520        19.390683         0.806000          0.894457   
20      32.917740        15.571428         0.823981          0.914602   
21      35.612069        16.558654         0.822186          0.907621   
22      39.417994        17.714181         0.816998          0.903759   
23      41.206913        18.178153         0.813566          0.902316   
24      37.729851        17.667012         0.819652          0.910429   
25      42.304446        19.199188         0.815165          0.901107   
26      43.712365        19.338221         0.808730          0.896037   
27      46.754226        20.132737         0.804362          0.894028   
28      36.521724        16.675611         0.824995          0.916026   
29      39.063008        17.705877         0.821874          0.907913   
..            ...              ...              ...               ...   
42      36.291567        16.581818         0.817779          0.903701   
43      39.155814        17.177200         0.815906          0.903135   
44      28.109436        13.443637         0.823124          0.917216   
45      30.850932        14.368191         0.827178          0.913510   
46      34.090768        15.446172         0.825111          0.911111   
47      36.981059        16.067337         0.822420          0.910409   
48      32.795083        15.468537         0.823279          0.914563   
49      35.758747        16.661179         0.820197          0.905124   
50      38.941349        17.590703         0.814502          0.901614   
51      41.849347        18.162512         0.811265          0.900268   
52      30.977614        14.482858         0.827375          0.918873   
53      33.869955        15.597917         0.826008          0.912867   
54      37.011546        16.617730         0.821405          0.908635   
55      39.882440        17.326067         0.818090          0.907543   
56      36.353434        16.468678         0.824098          0.915831   
57      39.353178        17.497012         0.819026          0.906158   
58      42.414251        18.470642         0.813176          0.901614   
59      45.085803        19.105772         0.810134          0.900015   
60      34.019043        15.377444         0.827998          0.920336   
61      36.863596        16.538026         0.825150          0.913198   
62      40.286285        17.391757         0.820352          0.908537   
63      43.390237        18.155932         0.816608          0.907016   
64      39.166489        17.169406         0.823396          0.917293   
65      41.835323        18.092930         0.817973          0.907094   
66      45.561112        19.363963         0.812435          0.902375   
67      47.940881        19.804314         0.809315          0.900522   
68      36.821276        16.192119         0.827803          0.921603   
69      39.696340        17.262112         0.825033          0.914095   
70      43.335725        18.193642         0.819104          0.909239   
71      51.402930        31.122842         0.816179          0.907659   

   param_chars__c_ngrams__cvect__ngram_range  \
0                                     (1, 2)   
1                                     (1, 2)   
2                                     (1, 2)   
3                                     (1, 2)   
4                                     (1, 2)   
5                                     (1, 2)   
6                                     (1, 2)   
7                                     (1, 2)   
8                                     (1, 3)   
9                                     (1, 3)   
10                                    (1, 3)   
11                                    (1, 3)   
12                                    (1, 3)   
13                                    (1, 3)   
14                                    (1, 3)   
15                                    (1, 3)   
16                                    (1, 4)   
17                                    (1, 4)   
18                                    (1, 4)   
19                                    (1, 4)   
20                                    (1, 4)   
21                                    (1, 4)   
22                                    (1, 4)   
23                                    (1, 4)   
24                                    (1, 5)   
25                                    (1, 5)   
26                                    (1, 5)   
27                                    (1, 5)   
28                                    (1, 5)   
29                                    (1, 5)   
..                                       ...   
42                                    (2, 3)   
43                                    (2, 3)   
44                                    (2, 3)   
45                                    (2, 3)   
46                                    (2, 3)   
47                                    (2, 3)   
48                                    (2, 4)   
49                                    (2, 4)   
50                                    (2, 4)   
51                                    (2, 4)   
52                                    (2, 4)   
53                                    (2, 4)   
54                                    (2, 4)   
55                                    (2, 4)   
56                                    (2, 5)   
57                                    (2, 5)   
58                                    (2, 5)   
59                                    (2, 5)   
60                                    (2, 5)   
61                                    (2, 5)   
62                                    (2, 5)   
63                                    (2, 5)   
64                                    (2, 6)   
65                                    (2, 6)   
66                                    (2, 6)   
67                                    (2, 6)   
68                                    (2, 6)   
69                                    (2, 6)   
70                                    (2, 6)   
71                                    (2, 6)   

   param_words_chars__wc_ngrams__featureunion__char_ngrams__cvect__ngram_range  \
0                                              (1, 3)                            
1                                              (1, 4)                            
2                                              (1, 5)                            
3                                              (1, 6)                            
4                                              (2, 3)                            
5                                              (2, 4)                            
6                                              (2, 5)                            
7                                              (2, 6)                            
8                                              (1, 3)                            
9                                              (1, 4)                            
10                                             (1, 5)                            
11                                             (1, 6)                            
12                                             (2, 3)                            
13                                             (2, 4)                            
14                                             (2, 5)                            
15                                             (2, 6)                            
16                                             (1, 3)                            
17                                             (1, 4)                            
18                                             (1, 5)                            
19                                             (1, 6)                            
20                                             (2, 3)                            
21                                             (2, 4)                            
22                                             (2, 5)                            
23                                             (2, 6)                            
24                                             (1, 3)                            
25                                             (1, 4)                            
26                                             (1, 5)                            
27                                             (1, 6)                            
28                                             (2, 3)                            
29                                             (2, 4)                            
..                                                ...                            
42                                             (1, 5)                            
43                                             (1, 6)                            
44                                             (2, 3)                            
45                                             (2, 4)                            
46                                             (2, 5)                            
47                                             (2, 6)                            
48                                             (1, 3)                            
49                                             (1, 4)                            
50                                             (1, 5)                            
51                                             (1, 6)                            
52                                             (2, 3)                            
53                                             (2, 4)                            
54                                             (2, 5)                            
55                                             (2, 6)                            
56                                             (1, 3)                            
57                                             (1, 4)                            
58                                             (1, 5)                            
59                                             (1, 6)                            
60                                             (2, 3)                            
61                                             (2, 4)                            
62                                             (2, 5)                            
63                                             (2, 6)                            
64                                             (1, 3)                            
65                                             (1, 4)                            
66                                             (1, 5)                            
67                                             (1, 6)                            
68                                             (2, 3)                            
69                                             (2, 4)                            
70                                             (2, 5)                            
71                                             (2, 6)                            

                                               params  rank_test_score  \
0   {'chars__c_ngrams__cvect__ngram_range': (1, 2)...               69   
1   {'chars__c_ngrams__cvect__ngram_range': (1, 2)...               60   
2   {'chars__c_ngrams__cvect__ngram_range': (1, 2)...               67   
3   {'chars__c_ngrams__cvect__ngram_range': (1, 2)...               70   
4   {'chars__c_ngrams__cvect__ngram_range': (1, 2)...               58   
5   {'chars__c_ngrams__cvect__ngram_range': (1, 2)...               33   
6   {'chars__c_ngrams__cvect__ngram_range': (1, 2)...               50   
7   {'chars__c_ngrams__cvect__ngram_range': (1, 2)...               55   
8   {'chars__c_ngrams__cvect__ngram_range': (1, 3)...               41   
9   {'chars__c_ngrams__cvect__ngram_range': (1, 3)...               34   
10  {'chars__c_ngrams__cvect__ngram_range': (1, 3)...               53   
11  {'chars__c_ngrams__cvect__ngram_range': (1, 3)...               62   
12  {'chars__c_ngrams__cvect__ngram_range': (1, 3)...               23   
13  {'chars__c_ngrams__cvect__ngram_range': (1, 3)...               11   
14  {'chars__c_ngrams__cvect__ngram_range': (1, 3)...               28   
15  {'chars__c_ngrams__cvect__ngram_range': (1, 3)...               38   
16  {'chars__c_ngrams__cvect__ngram_range': (1, 4)...               25   
17  {'chars__c_ngrams__cvect__ngram_range': (1, 4)...               45   
18  {'chars__c_ngrams__cvect__ngram_range': (1, 4)...               63   
19  {'chars__c_ngrams__cvect__ngram_range': (1, 4)...               68   
20  {'chars__c_ngrams__cvect__ngram_range': (1, 4)...               13   
21  {'chars__c_ngrams__cvect__ngram_range': (1, 4)...               19   
22  {'chars__c_ngrams__cvect__ngram_range': (1, 4)...               39   
23  {'chars__c_ngrams__cvect__ngram_range': (1, 4)...               51   
24  {'chars__c_ngrams__cvect__ngram_range': (1, 5)...               30   
25  {'chars__c_ngrams__cvect__ngram_range': (1, 5)...               47   
26  {'chars__c_ngrams__cvect__ngram_range': (1, 5)...               65   
27  {'chars__c_ngrams__cvect__ngram_range': (1, 5)...               71   
28  {'chars__c_ngrams__cvect__ngram_range': (1, 5)...                9   
29  {'chars__c_ngrams__cvect__ngram_range': (1, 5)...               20   
..                                                ...              ...   
42  {'chars__c_ngrams__cvect__ngram_range': (2, 3)...               37   
43  {'chars__c_ngrams__cvect__ngram_range': (2, 3)...               44   
44  {'chars__c_ngrams__cvect__ngram_range': (2, 3)...               16   
45  {'chars__c_ngrams__cvect__ngram_range': (2, 3)...                4   
46  {'chars__c_ngrams__cvect__ngram_range': (2, 3)...                7   
47  {'chars__c_ngrams__cvect__ngram_range': (2, 3)...               17   
48  {'chars__c_ngrams__cvect__ngram_range': (2, 4)...               15   
49  {'chars__c_ngrams__cvect__ngram_range': (2, 4)...               27   
50  {'chars__c_ngrams__cvect__ngram_range': (2, 4)...               48   
51  {'chars__c_ngrams__cvect__ngram_range': (2, 4)...               59   
52  {'chars__c_ngrams__cvect__ngram_range': (2, 4)...                3   
53  {'chars__c_ngrams__cvect__ngram_range': (2, 4)...                5   
54  {'chars__c_ngrams__cvect__ngram_range': (2, 4)...               22   
55  {'chars__c_ngrams__cvect__ngram_range': (2, 4)...               35   
56  {'chars__c_ngrams__cvect__ngram_range': (2, 5)...               12   
57  {'chars__c_ngrams__cvect__ngram_range': (2, 5)...               32   
58  {'chars__c_ngrams__cvect__ngram_range': (2, 5)...               52   
59  {'chars__c_ngrams__cvect__ngram_range': (2, 5)...               61   
60  {'chars__c_ngrams__cvect__ngram_range': (2, 5)...                1   
61  {'chars__c_ngrams__cvect__ngram_range': (2, 5)...                6   
62  {'chars__c_ngrams__cvect__ngram_range': (2, 5)...               24   
63  {'chars__c_ngrams__cvect__ngram_range': (2, 5)...               42   
64  {'chars__c_ngrams__cvect__ngram_range': (2, 6)...               14   
65  {'chars__c_ngrams__cvect__ngram_range': (2, 6)...               36   
66  {'chars__c_ngrams__cvect__ngram_range': (2, 6)...               56   
67  {'chars__c_ngrams__cvect__ngram_range': (2, 6)...               64   
68  {'chars__c_ngrams__cvect__ngram_range': (2, 6)...                2   
69  {'chars__c_ngrams__cvect__ngram_range': (2, 6)...                8   
70  {'chars__c_ngrams__cvect__ngram_range': (2, 6)...               31   
71  {'chars__c_ngrams__cvect__ngram_range': (2, 6)...               43   

    split0_test_score  split0_train_score  split1_test_score  \
0            0.736313            0.902229           0.893166   
1            0.772227            0.898309           0.877018   
2            0.774216            0.893570           0.868008   
3            0.773514            0.892458           0.863328   
4            0.742747            0.906149           0.899953   
5            0.780299            0.904628           0.889305   
6            0.781469            0.901176           0.877018   
7            0.782990            0.900064           0.872455   
8            0.761816            0.908373           0.896560   
9            0.786617            0.902814           0.882752   
10           0.783224            0.898426           0.871168   
11           0.780416            0.897899           0.866253   
12           0.765208            0.912761           0.902293   
13           0.793402            0.909309           0.890358   
14           0.794104            0.905623           0.878657   
15           0.790360            0.904628           0.875848   
16           0.776907            0.907729           0.892347   
17           0.786734            0.900942           0.873859   
18           0.782873            0.894623           0.862626   
19           0.780767            0.892692           0.856775   
20           0.779130            0.912995           0.897262   
21           0.795274            0.906910           0.881933   
22           0.792349            0.901820           0.872689   
23           0.788606            0.899889           0.867540   
24           0.779715            0.908841           0.888486   
25           0.787670            0.899889           0.871402   
26           0.782873            0.893102           0.859583   
27           0.779832            0.890469           0.851392   
28           0.785096            0.913522           0.895039   
29           0.795976            0.906208           0.880529   
..                ...                 ...                ...   
42           0.788255            0.903224           0.877955   
43           0.787787            0.901820           0.874444   
44           0.767080            0.915277           0.906740   
45           0.795274            0.912703           0.894688   
46           0.799719            0.909309           0.886263   
47           0.797496            0.908139           0.881816   
48           0.780066            0.912995           0.896677   
49           0.793636            0.904921           0.878891   
50           0.789190            0.900064           0.869647   
51           0.786149            0.898134           0.864264   
52           0.783107            0.917032           0.901591   
53           0.797964            0.911298           0.888954   
54           0.798432            0.905915           0.878774   
55           0.794455            0.904862           0.873508   
56           0.785447            0.913873           0.893868   
57           0.794104            0.904394           0.876316   
58           0.787436            0.898485           0.866019   
59           0.785096            0.895852           0.860871   
60           0.788021            0.918027           0.899602   
61           0.801474            0.910362           0.884039   
62           0.797263            0.904804           0.875497   
63           0.791881            0.903224           0.870349   
64           0.785213            0.914809           0.892932   
65           0.792466            0.904511           0.874678   
66           0.786734            0.898309           0.864732   
67           0.786851            0.895969           0.858062   
68           0.789307            0.918788           0.897964   
69           0.802059            0.910362           0.882986   
70           0.794455            0.905272           0.874210   
71           0.793051            0.902990           0.867891   

    split1_train_score  split2_test_score  split2_train_score  std_fit_time  \
0             0.881706           0.786893            0.921376      0.472080   
1             0.871936           0.783148            0.921844      0.622632   
2             0.868075           0.781159            0.922078      0.574954   
3             0.865676           0.779286            0.922897      0.918412   
4             0.891535           0.791691            0.926582      0.582424   
5             0.880477           0.786074            0.929273      0.520242   
6             0.878839           0.784084            0.930736      0.523684   
7             0.876558           0.783031            0.930911      0.715238   
8             0.888668           0.791457            0.929741      0.373387   
9             0.876382           0.785957            0.927635      0.754162   
10            0.872579           0.784435            0.927050      1.170917   
11            0.869537           0.782446            0.926758      0.928272   
12            0.895630           0.794032            0.933485      0.461241   
13            0.885567           0.789233            0.934363      0.615244   
14            0.881238           0.787361            0.934480      0.643021   
15            0.879892           0.785372            0.933953      0.751942   
16            0.886152           0.791457            0.933251      0.687503   
17            0.874335           0.786191            0.927811      0.654224   
18            0.868250           0.783616            0.925997      0.896190   
19            0.865910           0.780456            0.924769      1.303963   
20            0.893699           0.795553            0.937112      0.797208   
21            0.881647           0.789350            0.934304      0.412344   
22            0.877260           0.785957            0.932198      1.457570   
23            0.874978           0.784552            0.932081      0.787424   
24            0.886620           0.790755            0.935825      0.742120   
25            0.874159           0.786425            0.929273      1.090440   
26            0.868543           0.783733            0.926465      1.218376   
27            0.865793           0.781861            0.925822      1.431678   
28            0.894635           0.794851            0.939920      0.712532   
29            0.882759           0.789116            0.934772      1.051340   
..                 ...                ...                 ...           ...   
42            0.877377           0.787127            0.930502      0.890517   
43            0.876441           0.785489            0.931145      0.830328   
44            0.900135           0.795553            0.936235      0.446755   
45            0.890950           0.791574            0.936878      0.425216   
46            0.886679           0.789350            0.937346      0.513811   
47            0.885041           0.787946            0.938048      0.778678   
48            0.893173           0.793095            0.937522      0.627563   
49            0.878898           0.788063            0.931555      0.624347   
50            0.874627           0.784669            0.930151      0.779718   
51            0.872755           0.783382            0.929917      0.862140   
52            0.899081           0.797425            0.940505      0.606420   
53            0.888668           0.791106            0.938633      0.682980   
54            0.883110           0.787010            0.936878      0.743443   
55            0.881004           0.786308            0.936761      0.831688   
56            0.893582           0.792978            0.940037      1.234875   
57            0.880594           0.786659            0.933485      0.602210   
58            0.874510           0.786074            0.931847      0.909097   
59            0.873223           0.784435            0.930970      0.956451   
60            0.899842           0.796372            0.943138      0.591735   
61            0.888726           0.789936            0.940505      0.734953   
62            0.882818           0.788297            0.937990      0.848460   
63            0.880711           0.787595            0.937112      1.064291   
64            0.895045           0.792042            0.942026      0.931966   
65            0.881062           0.786776            0.935708      0.871108   
66            0.876090           0.785840            0.932725      0.881466   
67            0.873691           0.783031            0.931906      1.043519   
68            0.901305           0.796138            0.944717      0.677764   
69            0.889428           0.790053            0.942494      0.753332   
70            0.883695           0.788648            0.938750      1.124935   
71            0.881823           0.787595            0.938165      6.522718   

    std_score_time  std_test_score  std_train_score  
0         0.438293        0.065367         0.016198  
1         0.462688        0.047037         0.020386  
2         0.448393        0.042672         0.022058  
3         0.636019        0.041046         0.023376  
4         0.223789        0.065684         0.014374  
5         0.434636        0.050080         0.019921  
6         0.412615        0.044439         0.021255  
7         0.471267        0.042164         0.022257  
8         0.521266        0.057813         0.016773  
9         0.557518        0.045475         0.020928  
10        0.534786        0.041175         0.022247  
11        0.826524        0.039994         0.023361  
12        0.446025        0.059014         0.015478  
13        0.498586        0.046719         0.019923  
14        0.660240        0.041539         0.021762  
15        0.706868        0.041525         0.022097  
16        0.501562        0.051334         0.019251  
17        0.675563        0.041200         0.021832  
18        0.754387        0.037422         0.023605  
19        0.970351        0.035904         0.024061  
20        0.395877        0.052249         0.017760  
21        0.566011        0.042317         0.021503  
22        1.031777        0.039466         0.022471  
23        0.667050        0.038201         0.023375  
24        0.192686        0.048881         0.020119  
25        1.233966        0.039768         0.022517  
26        0.688212        0.035961         0.023738  
27        0.730870        0.033266         0.024636  
28        0.833273        0.049688         0.018572  
29        0.888024        0.041570         0.021269  
..             ...             ...              ...  
42        0.609471        0.042553         0.021691  
43        0.745412        0.041403         0.022352  
44        0.435506        0.060257         0.014802  
45        0.398910        0.047760         0.018759  
46        0.587436        0.043448         0.020724  
47        0.648586        0.042180         0.021700  
48        0.471688        0.052172         0.018139  
49        0.598758        0.041565         0.021498  
50        0.716349        0.039037         0.022694  
51        0.746783        0.037493         0.023385  
52        0.482564        0.052804         0.016961  
53        0.576369        0.044597         0.020428  
54        0.578418        0.040833         0.022035  
55        0.777003        0.039327         0.022842  
56        0.576133        0.049431         0.019016  
57        0.631329        0.040624         0.021629  
58        0.712647        0.037370         0.023512  
59        0.810092        0.035877         0.023758  
60        0.487529        0.050746         0.017751  
61        0.673883        0.041907         0.021234  
62        0.709309        0.039165         0.022678  
63        0.724078        0.038040         0.023181  
64        0.616959        0.049249         0.019260  
65        0.495065        0.040164         0.022384  
66        0.838309        0.036981         0.023299  
67        0.764957        0.034505         0.023983  
68        0.594963        0.049689         0.017835  
69        0.759845        0.041271         0.021824  
70        0.782217        0.039038         0.022651  
71        8.364699        0.036634         0.023237  

[72 rows x 18 columns]
